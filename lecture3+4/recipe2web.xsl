<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:r="http://xmllab.fhv.at/recipes"
>
	
	<xsl:template match="/">
		<html>
			<head>
				<title><xsl:value-of select="r:collection/r:description"/></title>
			</head>
			<body>
				<h1><xsl:value-of select="r:collection/r:description"/></h1>
				
				<!-- Table of Contents -->
				<h2>Table of Contents</h2>
				<ol>
					<xsl:apply-templates select="r:collection/r:recipe"  mode="toc"/>
				</ol>
				
				<!-- Details of all recipes -->
				<xsl:apply-templates select="r:collection/r:recipe" mode="detail"/>
				
				<!-- Index of ingredients -->
				<h2>Index of Ingredients</h2>
				<ul>
					<xsl:for-each-group select="r:collection/r:recipe/r:ingredient" group-by="@name">
						<xsl:sort select="upper-case(@name)"/>
						<li>
							<xsl:value-of select="current-grouping-key()"/>
							<xsl:if test="count(current-group()) > 1">
								<ul>
									<xsl:for-each select="current-group()">
										<li><xsl:value-of select="@name"/></li>
									</xsl:for-each>
								</ul>
							</xsl:if>
						</li>
					</xsl:for-each-group>
				</ul>
			</body>
		</html>
	</xsl:template>
	
	<xsl:template match="r:recipe" mode="toc">
		<li><a href="#{generate-id()}"><xsl:value-of select="r:title"/></a></li>
	</xsl:template>
	
	<xsl:template match="r:recipe" mode="detail">
		<h2 id="{generate-id()}"><xsl:value-of select="r:title"/></h2>
		<img src="{r:image}" height="120px"/>
		<h3>Ingedients</h3>
		<ul>
			<xsl:for-each select="r:ingredient" xml:space="preserve">
				<li>
					<xsl:choose>
						<xsl:when test="@amount = 1">one</xsl:when>
						<xsl:when test="@amount = 2">two</xsl:when>
						<xsl:when test="@amount = 3">three</xsl:when>
						<xsl:when test="@amount = 4">four</xsl:when>
						<xsl:when test="@amount = 5">five</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="@amount"/>
						</xsl:otherwise>
					</xsl:choose>
					<xsl:value-of select="@unit"/>
					<xsl:value-of select="@name"/>
				</li>
			</xsl:for-each>
		</ul>
		
		<h3>Preparation</h3>
		<ol>
			<xsl:for-each select="r:preparation/r:step">
				<li><xsl:value-of select="."/></li>
			</xsl:for-each>
		</ol>
	</xsl:template>
	
	
</xsl:stylesheet>