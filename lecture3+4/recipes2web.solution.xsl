<?xml version="1.0" encoding="UTF-8"?>
<!-- Wir benötigen in diesem stylesheet elemente aus den drei namespaces für xsl, xhtml -->
<!-- und unsere Recipes. Für xhtml nehmen wir den default namespace, für die anderen -->
<!-- beiden verwenden wir die Abkürzungen xsl und rcp. -->
<xsl:stylesheet version="1.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:rcp="http://xmllab.fhv.at/recipes">

  <xsl:output method="html" />
  
<!-- Für die Auflösung der author-id Referenzen bei den recipe-Elementen bauen wir einen -->
<!-- Index auf. Dieser besteht aus key/value Paaren, wobei die values alle author-Elemente -->
<!-- im Dokument sind und die keys ihre id-Attribute. // steht für "in beliebiger Tiefe".   -->
  <xsl:key name="author-index" match="//rcp:author" use="@id" />
  

<!-- Dies ist das root-template. Hier startet der Transformationsprozess. Der context ist die -->
<!-- root des XML Dokumentes (nicht zu verwechseln mit dem root-Element!) -->
  <xsl:template match="/">
    <html>
      <head>
        <title>
          <xsl:value-of select="rcp:collection/rcp:description" />
        </title>
      </head>
      <body>
        <h1>
          <xsl:value-of select="rcp:collection/rcp:description" />
        </h1>
        
        <h2>Table of Contents</h2>
        <ol>
<!--         Mit apply-templates werden die passenden templates aus dem stylesheet auf die -->
<!--         ausgewählten Elemente, in diesem Fall alle recipe-Elemente, angewandt. Beim folgenden -->
<!--         Aufruf wird das 2. template des stylsheets auf jedes der fünf recipe-Elemente angewandt. -->
          <xsl:apply-templates select="rcp:collection/rcp:recipe" mode="table-of-contents"/>
          <li><a href="#index">Index of Ingredients</a></li>
        </ol>
        
        <!-- recipe details -->
<!--         Bei diesem Aufruf von apply-templates kommt das 3. template des stylesheets zum Zuge. -->
        <xsl:apply-templates select="rcp:collection/rcp:recipe" mode="detail"/>
        
        <h2 id="index">Index of Ingredients</h2>
        <table>
          <tr><th>ingredient</th><th>recipe</th></tr>
          
<!--           Iteration über alle ingredient-Elemente des Inputs. Mit "//" werden Elemente in -->
<!--           beliebiger Verschachtelungstiefe ausgewählt. -->
          <xsl:for-each select="//rcp:ingredient">
<!--           Innerhalb des for-each Elementes ist der context das aktuelle Element der Iteration. -->
<!--           Mit dem direkt auf for-each folgenden sort-Element kann die Menge, über die iteriert -->
<!--           wird, sortiert werden. -->
            <xsl:sort select="@name" />
            <tr>
<!--               Mit @name greift man auf das Attribut "name" des context-Elementes zu. -->
              <td><xsl:value-of select="@name" /></td>
<!--               Mit ancestor::rcp:recipe geht man vom context soweit nach außen, bis ein -->
<!--               recipe-element angetroffen wird.   -->
              <td><xsl:value-of select="ancestor::rcp:recipe/rcp:title" /></td>
            </tr>            
          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>
  
  
<!-- Dieses template wird auf recipe-Elemente angewandt, wenn der mode table-of-contents gewähkt wird. -->
  <xsl:template match="rcp:recipe" mode="table-of-contents">
<!--   Der context ist innerhalb des templates ein recipe-Element. -->
<!--   Mit den Klammnern {...} kann ein XPath Ausdruck in einem Attribut-Wert verwendet werden. -->
<!--   Die folgende XPath-Funktion generate-id() generiert beim ersten Aufruf ein id-Attribut mit einem -->
<!--   zufälligen, aber eindeutigen Wert beim recipe-Element des inputs. -->
<!--   Der return-value beim ertsen, wie auch bei jedem folgenden Aufruf, ist dann der generierte Wert.  -->
    <li><a href="#{generate-id()}"><xsl:value-of select="rcp:title" /></a></li>
  </xsl:template>


  <xsl:template match="rcp:recipe" mode="detail">
  
    <xsl:if test="position() > 1">
      <hr />
    </xsl:if>
    
<!--     Hier ist der context wiederum ein recipe-Element. Nachdem dieses template nach dem 2. template -->
<!--     zum Zuge kommt, sind die ids für die recipe-Elemente bereits vergeben und generate-id() -->
<!--     liefert die entsprechenden eindeutigen Werte zurück. -->
    <h2 id="{generate-id()}"><xsl:value-of select="rcp:title" /></h2>
    
    <xsl:if test="@author-id">
		<div>Author: <xsl:value-of select="key('author-index', @author-id)"/></div>
	</xsl:if>
    
 	<img src="{rcp:image}" height="120px"/>
    
    <h3>Ingredients</h3>
<!--     call-template entspricht einem Funktionsaufruf. Bei können auch Parameter übergeben werden. -->
    <xsl:call-template name="list-of-ingredients">
      <xsl:with-param name="i" select="rcp:ingredient" />
    </xsl:call-template>
    
    <h3>Preparation</h3>
    <xsl:call-template name="list-of-preparations">
      <xsl:with-param name="preps" select="rcp:preparation/rcp:step" />
    </xsl:call-template>
  </xsl:template>
  
  
<!--   Durch Verwendung des "name" Attrubutes beim template-Element kann ein template per -->
<!--   call-template wie eine Funkion aufgerufen werden. Allfällige Parameter werden als unmittelbar -->
<!--   folgenden child-Elemente definiert. -->
  <xsl:template name="list-of-preparations">
    <xsl:param name="preps" />
    <ol>
<!--       Der Parameter "preps" wird durch $preps in einem XPAth-Ausdruck angesprochen. -->
      <xsl:for-each select="$preps">
<!--       Im loop ist der context jeweils das aktuelle Iterationselement, in unserem Fall ein -->
<!--       step-Element. "." steht im XPAth-Asudruck für das current-element, im Falle eines -->
<!--       step-Elementes evaluiert das auf seinen Text-Inhalt.  -->
        <li><xsl:value-of select="." /></li>
      </xsl:for-each>
    </ol>
  </xsl:template>


  <xsl:template name="list-of-ingredients">
    <xsl:param name="i" />
    <ul>
      <xsl:for-each select="$i">
<!--       Das choose-Element entspricht einem Java switch. -->
        <xsl:choose>
        
          <xsl:when test="count(rcp:ingredient) > 0">
            <li>
              <div style="font-weight: bold"><xsl:value-of select="@name" /></div>
              <div>For the <xsl:value-of select="@name" /> you need:</div>
              <xsl:call-template name="list-of-ingredients" >
                <xsl:with-param name="i" select="rcp:ingredient" />
              </xsl:call-template>
              <div>Prepare the <xsl:value-of select="@name" />:</div>
              <xsl:call-template name="list-of-preparations">
                <xsl:with-param name="preps" select="rcp:preparation/rcp:step" />
              </xsl:call-template>
            </li>
          </xsl:when>
          
          <xsl:otherwise>
<!--           Hier ist es wichtig, dass der whitespace zwischen den einzelnen Werten erhalten bleibt. -->
<!--           Der XSLT prozessor stripped sonst den white-space im output. Das läßt sich mit -->
<!--           dem attribut xml:space="preserve" steuern. -->
            <li xml:space="preserve">
            	<xsl:value-of select="@amount" />
            	<xsl:value-of select="@unit" />
            	<xsl:value-of select="@name" />
            </li>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:for-each>
    </ul>
  </xsl:template>


</xsl:stylesheet>
