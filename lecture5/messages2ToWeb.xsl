<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:m="http://xmllab.fhv.at/messages2">

	<xsl:output method="html" indent="yes"/>

	<xsl:template match="/">
		<html>
			<head>
				<title>XMLLab Example "messages2"</title>
			</head>
			<body>
				<xsl:apply-templates select="/m:messages/m:message"/>
			</body>
		</html>
	</xsl:template>
	
	<xsl:template match="m:message">
		<b><xsl:value-of select="@from"/></b><br/>
		to: <xsl:value-of select="@to"/><br/>
		subject: <xsl:value-of select="@subject"/><br/>

		<p><xsl:value-of select="m:text"/></p>
		<dl>
			<xsl:for-each select="m:in-reply-to">
				<dt><xsl:value-of select="@from"/></dt>
				<dd><xsl:value-of select="m:text"/></dd>
			</xsl:for-each>
		</dl>
		
	</xsl:template>
</xsl:stylesheet>